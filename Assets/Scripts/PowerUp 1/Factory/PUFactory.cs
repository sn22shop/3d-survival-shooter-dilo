﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PUFactory : MonoBehaviour, IFactoryPowerUp
{

    [SerializeField]
    public GameObject[] puPrefab;

    public GameObject FactoryMethodPU   (int puTag)
    {
        GameObject pu = Instantiate(puPrefab[puTag]);
        return pu;
    }
}
