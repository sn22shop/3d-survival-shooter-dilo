﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Heal : MonoBehaviour
{
    public int healAdd = 10;

    GameObject player;
    PlayerHealth playerHealth;
    bool playerInRange;

    // Start is called before the first frame update
    void Start()
    {
        int randSpawnX = Random.Range(0, 15);
        int randSpawnZ = Random.Range(0, 15);
        gameObject.transform.position = new Vector3(randSpawnX, 2, randSpawnZ);
    }


    void Awake()
    {
        player = GameObject.FindGameObjectWithTag("Player");
        playerHealth = player.GetComponent<PlayerHealth>();

    }
    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject == player && other.isTrigger == false)
        {
            playerInRange = true;

        }
    }

    void OnTriggerExit(Collider other)
    {
        if (other.gameObject == player)
        {
            playerInRange = false;
        }
    }
    void Update()
    {

        if (playerInRange)
        {
            Addheal();
        }

    }
    void Addheal()
    {
        if (playerHealth.currentHealth > 0)
        {
            playerHealth.AddHealth(healAdd);
            Destroy(gameObject);
        }
    }
}
