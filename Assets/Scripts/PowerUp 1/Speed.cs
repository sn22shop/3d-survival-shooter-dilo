﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Speed : MonoBehaviour
{
    public int durSpeed = 2;

    GameObject player;
    PlayerHealth playerHealth;
    bool playerInRange;
    PlayerMovement playerMovement;

    // Start is called before the first frame update
    void Start()
    {
        int randSpawnX = Random.Range(0, 15);
        int randSpawnZ = Random.Range(0, 15);
        gameObject.transform.position = new Vector3(randSpawnX, 2, randSpawnZ);
    }


    void Awake()
    {
        player = GameObject.FindGameObjectWithTag("Player");
        playerHealth = player.GetComponent<PlayerHealth>();
        playerMovement = player.GetComponent<PlayerMovement>();

    }
    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject == player && other.isTrigger == false)
        {
            playerInRange = true;

        }
    }

    void OnTriggerExit(Collider other)
    {
        if (other.gameObject == player)
        {
            playerInRange = false;
        }
    }
    void Update()
    {

        if (playerInRange)
        {
            SpeedUp();
        }


    }
    void SpeedUp()
    {
        if (playerHealth.currentHealth > 0)
        {
            playerMovement.SpeedUp(durSpeed);
            Destroy(gameObject);
        }
    }
}
