﻿using UnityEngine;

public class PowerUpManager : MonoBehaviour
{
    public PlayerHealth playerHealth;
    public float spawnTime = 3f;
    public Transform[] spawnPoints;

    [SerializeField]
    MonoBehaviour factory;
    IFactoryPowerUp Factory { get { return factory as IFactoryPowerUp; } }

    void Start()
    {
        //eksekusi fungsi spawn sesuai spawntime
        InvokeRepeating("Spawn", spawnTime, spawnTime);
    }

    void Spawn()
    {
        print("testmanager");
        //duplikasi powerup item
            Factory.FactoryMethodPU(0);
    }
}