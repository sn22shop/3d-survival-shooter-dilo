﻿using UnityEngine;

public class EnemyManager : MonoBehaviour
{
    public PlayerHealth playerHealth;
    public float spawnTime = 3f;
    public Transform[] spawnPoints;

    [SerializeField]
    MonoBehaviour factory;
    IFactoryEnemy Factory { get { return factory as IFactoryEnemy; } }

    void Start(){
        //eksekusi fungsi spawn sesuai spawntime
        InvokeRepeating("Spawn", spawnTime, spawnTime);
    }

    void Spawn(){
        //mati = tidak generate enemy baru
        if (playerHealth.currentHealth <= 0f){
           return;

            print("ret");
        }

        print("testmanager");
        //Memduplikasi enemy
        Factory.FactoryMethod(0);
    }
}