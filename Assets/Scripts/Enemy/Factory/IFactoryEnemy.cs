using UnityEngine;

public interface IFactoryEnemy
{
    GameObject FactoryMethod(int enemyTag);
}
