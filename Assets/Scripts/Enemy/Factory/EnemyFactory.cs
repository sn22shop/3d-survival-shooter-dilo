using System;
using UnityEngine;

public class EnemyFactory : MonoBehaviour, IFactoryEnemy{

    [SerializeField]
    public GameObject[] enemyPrefab;

    public GameObject FactoryMethod(int enemyTag)
    {
        GameObject enemy = Instantiate(enemyPrefab[enemyTag]);
        return enemy;
    }
}